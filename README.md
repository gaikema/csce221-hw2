# Homework 2
Code for CSCE221 homework 4.
Assignment [here](https://www.dropbox.com/home/CSCE%20221/Homework%202?preview=221-16a-h2.pdf).

---

# Instructions

## Linux
Switch to the folder `221hw2` and type `make` to compile, then type `make run` to run it.
Type `make clean` to remove the object files.

## Windows
Open the [sln](https://bitbucket.org/gaikema/csce221-hw2/src/66fbbb530b49333c9893a9ffa201fda53220bb99/221hw2.sln?at=master&fileviewer=file-view-default) file in Visual Studio.

---

# Downloads

---

# Miscellaneous

## R scripts
* https://github.com/TexAgg/hash-function
