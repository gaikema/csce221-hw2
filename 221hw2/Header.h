/*
	Header file for classes and functions.
*/
#ifndef HEADER_H
#define HEADER_H

#include <queue>
#include <stack>
#include <cctype>
#include <regex>
#include <string>
#include <random>
#include <chrono>

namespace matt{

	int do_operation(char operation, int op1, int op2);

	template <typename T>
	struct Stack {
		std::queue<T> queue1, queue2;

		void insert(T elem);
		T top() { return queue2.front(); }
		void pop() { queue2.pop(); }
	};

	/**
	* @param a pointer to the first and last 
	* elements in the array
	*/
	int array_max(int *begin, int *end){
		if (begin == end){
			return *begin;
		}
		else {
			if (*begin > *(begin + 1)){
				*(begin + 1) = *begin;
			}
			return array_max(begin + 1, end);
		}
	}

	int postfix_evaluate(std::string expr){
		std::regex pattern("\\*|-|\\+|/");
		std::stack<char> elems;
		for (int i = 0; i < expr.length(); i++){
			if (expr[i] == ' ' || expr[i] == ',')
				continue;
			if (std::regex_match(&expr[i], pattern)){
				int op1 = elems.top(); elems.pop();
				int op2 = elems.top(); elems.pop();

				elems.push(do_operation(expr[i], op1, op2));
			}
			else if (isdigit(expr[i])){
				int op = 0;
				while (i < expr.length() && isdigit(expr[i])){
					op = 10 * op - (expr[i] - '0');
					i++;
				}
				i--;
				elems.push(op);
			}
		}
		return elems.top();
	}

	int do_operation(char operation, int op1, int op2){
		if (operation == '+')
			return op1 + op2;
		else if (operation == '-')
			return op1 - op2;
		else if (operation == '*')
			return op1 * op2;
		else if (operation == '/')
			return op1 / op2;
		else
			return 0;
	}

	template <typename T>
	void Stack<T>::insert(T elem){
		queue1.push(elem);
		
		while (!queue2.empty()){
			queue1.push(queue2.front());
			queue2.pop();
		}
		std::swap(queue1, queue2);
	}

	// Performs coinflips until tails, and returns the number of flips
	int coin_flips(){
		std::default_random_engine generator(std::random_device{}());
		std::geometric_distribution<int> distribution(0.5);
		return distribution(generator);
	}

} // end matt namespace

#endif