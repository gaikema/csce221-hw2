#include <iostream>
#include "Header.h"

using namespace std;

int main(){
	
	/*
	matt::Stack<int> stack;
	stack.insert(1);
	stack.insert(2);
	stack.insert(7);
	cout << stack.top() << endl;
	stack.pop();
	cout << stack.top() << endl;
	*/

	/*
	int foo[5] = { 1, 9, 3, 4, 8 };
	cout << matt::array_max(&foo[0], &foo[5]) << endl;
	*/

	/*
	cout << matt::postfix_evaluate("10 2 8 * + 3 -") << endl;
	*/

	cout << matt::coin_flips() + 1 << endl;

}